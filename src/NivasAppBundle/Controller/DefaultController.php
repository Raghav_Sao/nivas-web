<?php

namespace NivasAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
	/**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('NivasBundle:Default:index.html.twig');
    }
    
    /**
    * @Route("/insert",name = "add your property")
    */
    public function insertAction()
    {
    	return "ok";
    }

}
